import queue
import sys



def main():
    nodefile = sys.argv[2]
    edgefile = sys.argv[3]
    outputfile = sys.argv[4]
    nodetionary = {}
    node_to_id ={}
    id_to_node = {}
    node_file_parse(nodefile, node_to_id, id_to_node)
    edgeFileParse(edgefile, nodetionary)


    if sys.argv[1] == "-prune":
        prune(nodetionary, outputfile, int(sys.argv[5]))

    if sys.argv[1] == "-path":
        start = node_to_id[sys.argv[5]]
        end = node_to_id[sys.argv[6]]
        shortest_path(nodetionary, start, end, outputfile, id_to_node)

    if sys.argv[1] == "-cover":
        node = node_to_id[sys.argv[6]]
        k = int(sys.argv[5])
        remove_given_links(node,k, nodetionary, outputfile)


    # -prune data/nodes data/edges output1-1.txt 10000
# -prune data/nodes data/edges output1-2.txt 5000



def node_file_parse(inputfile, node_to_id, id_to_node):
    nodefile = open(inputfile)
    for line in nodefile:
        temp = line.split()
        node_to_id[temp[0]] = temp[1]
        id_to_node[temp[1]] = temp[0]
    nodefile.close()




def edgeFileParse(inputfile, nodetionary):
    edges = open(inputfile)
    for line in edges:
        temp = line.split()
        if temp[0] in nodetionary.keys():
            nodetionary[temp[0]][2].append(temp[1])
        else:
            nodetionary[temp[0]] = [0, [], []]
            nodetionary[temp[0]][2].append(temp[1])
        if temp[1] in nodetionary.keys():
            nodetionary[temp[1]][1].append(temp[0])
        else:
            nodetionary[temp[1]] = [0, [], []]
            nodetionary[temp[1]][1].append(temp[0])
    edges.close()


def prune(nodetionary, outputfile, k):
    s = ""

    for key in nodetionary.keys():
        if len(nodetionary[key][1]) + len(nodetionary[key][2]) >= k:
            print("loop")
            for value in nodetionary[key][1]:
                #print("inside the loop")
                s = s + key + "/t" + value + "/n"
                print(s)
    output = open(outputfile, 'w')
    output.write(s)
    output.close()


def shortest_path(nodetionary, start, end, outputfile, node_to_id):
    s = ""
    track = queue.Queue()
    track.put(list(start))
    nodetionary[start][0] = 1
    while not track.empty():
        route = track.get()
        current = route[len(list)]
        if current == end:
            return route
        else:
            for out_edges in nodetionary[current][1]:
                if not nodetionary[out_edges][0] == 1:
                    nodetionary[out_edges][0] = 1
                    temp = route.append(out_edges)
                    track.put(temp)
    for x in range(0, len(temp)-1):
        s = s + node_to_id[temp[x]] + "/t" + node_to_id[temp[x+1]] + "/n"
    output = open(outputfile, 'w')
    output.write(s)
    output.close()


def remove_given_links(node, k, nodetionary, outputfile):
    track = queue.Queue()
    s = ""
    track.put(list(node))
    nodetionary[node][0] = 1
    while not track.empty():
        route = track.get()
        current = route[len(route)]
        if len(route) == k:
            break
        else:
            for in_edges in nodetionary[current][2]:
                if not nodetionary[in_edges][0] ==1:
                    nodetionary[in_edges][0] = 1
                    temp = route.append(in_edges)
                    track.put(temp)

    for key in nodetionary.keys():
        if nodetionary[key][0] == 0:
            for out_edge in nodetionary[key][2]:
                if nodetionary[out_edge][0] == 0:
                    s = s + nodetionary[key] + "/t" + out_edge + "/n"

    output = open(outputfile, 'w')
    output.write(s)
    output.close()





main()















